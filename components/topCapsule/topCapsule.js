const app = getApp()
Component({
  data: {
    height: 0
  },
  attached: function () {
    // 获取是否是通过分享进入的小程序
    this.setData({
      share: app.globalData.share
    })
    // 定义导航栏的高度,方便对齐
    this.setData({
      height: app.globalData.height + 6
    })
  },
  methods: {
  // 返回上一页面
    navback() {
      wx.navigateBack()
    },
  //返回到首页
    backhome() {
      wx.switchTab({
        url: '/pages/index/index'
      })
    }
  }

}) 