// pages/orderDetail/orderDetail.js
import {getOrder} from '../../network/main';

Page({

  /**
   * 页面的初始数据
   */
  data: {
    orderDetail: {}
  },
  toProductDetail(event) {
    // console.log(event.currentTarget.id);
    // 静态数据
    wx.navigateTo({
      url: `/pages/productDetail/productDetail?type=${0}&id=${event.currentTarget.id}`
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let orderDetail = {};
    const {orderTimeStamp} = options;

    getOrder({
      orderTimeStamp
    }).then(res => {
      const {id, info, orderFlag, orderTime, products, totalPrice, orderTimeStamp} = res[0];
      orderDetail = {
        id,
        info: JSON.parse(info),
        orderFlag: JSON.parse(orderFlag),
        orderTime,
        products: JSON.parse(products),
        totalPrice,
        orderTimeStamp
      }
      // console.log(orderDetail);
      this.setData({
        orderDetail
      })
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    wx.switchTab({
      url: `/pages/manage/manage`
    })
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  }
})