// pages/searchPage/searchPage.js
import {getFlowerListByName} from '../../network/main';

Page({

  /**
   * 页面的初始数据
   */
  data: {
    searchText: '',
    offset: 0,
    size: 10,
    searchList: []
  },
  // 搜索框内容发送改变  
  onTextChange(e) {
    this.setData({
      searchText: e.detail
    });
  },
  // 点击搜索
  onTextSearch() {
    let input = this.data.searchText;
    if (input != '') {
        this.setData({
          offset: 0,
          searchList: []
        })
      this.getFlowerListByName();
    }
  },
  // 搜索数据请求
  getFlowerListByName() {
    getFlowerListByName({
      // 静态数据
      type: 0,
      flowerName: this.data.searchText,
      offset: this.data.offset,
      size: this.data.size
    }).then(res => {
      const {result, total} = res;

      for (let k in result) {
        result[k].cover = JSON.parse(result[k].imgPath)[0].filename;
      }
      
      this.setData({
        searchList: this.data.searchList.concat(result)
      })
    })
  },
  // 查看商品详情
  toProductDetail(data) {
    const {type, id} = data.currentTarget.dataset.index;
    wx.navigateTo({
      url: `/pages/productDetail/productDetail?type=${type}&id=${id}`
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
 
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    // 接近底部时改变搜索发送偏移量
    this.setData({
      offset: this.data.offset + 10,
    })
    this.getFlowerListByName();
  }
})