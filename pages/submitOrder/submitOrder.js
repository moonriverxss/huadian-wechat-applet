// pages/submitOrder/submitOrder.js
import {submitOrder} from '../../network/main';
import Toast from '@vant/weapp/toast/toast';

Page({

  /**
   * 页面的初始数据
   */
  data: {
    address: {},
    submitArrayData: [],
    submitPrice: 0
  },
  // 进入地址管理
  toAddressManage() {
    wx.switchTab({
      url: `/pages/manage/manage`
    })
  },
  // 查看商品详情
  toProductDetail(data) {
    const {type, id} = data.currentTarget.dataset.index;
    wx.navigateTo({
      url: `/pages/productDetail/productDetail?type=${type}&id=${id}`
    })
  },
  // 提交订单
  onSubmit() {
    const submitArrayData = this.data.submitArrayData;
    const shoppingCarList = wx.getStorageSync('shoppingCarList');
    
    if (!wx.getStorageSync('orderList')) {
      wx.setStorageSync('orderList', []);
    }
    if (!this.data.address.name == '') {
      Toast.loading({
        message: '提交订单中...',
        forbidClick: true,
      })
      const {name, phone, area, address} = this.data.address;
      const info = {name, phone, area, address};
      let orderTime = new Date().toLocaleString();
      let orderList = wx.getStorageSync('orderList');
      let orderTimeStamp = `${new Date().getTime()}${Math.floor((Math.random() * 100000) + 1)}`;

      submitOrder({
        info,
        products: this.data.submitArrayData,
        totalPrice: this.data.submitPrice,
        orderFlag: false,
        orderTime,
        orderTimeStamp
      }).then(res => {
        console.log(shoppingCarList);

        for (let k in submitArrayData) {
          let index = 0;
          for(let i in shoppingCarList){
            if(shoppingCarList[i].id == submitArrayData[k].id){
              index = i;
              break;
            }
          }
          shoppingCarList.splice(index,1);
        }
        console.log(shoppingCarList);
        wx.setStorageSync('shoppingCarList', shoppingCarList)
        
        Toast.success('提交成功！');
        // 最后一位会被抹零
        // console.log(res);
        orderList.unshift(orderTimeStamp);
        wx.setStorageSync('orderList', orderList);

        setTimeout(() => {
          wx.navigateTo({
            url: `/pages/orderDetail/orderDetail?orderTimeStamp=${orderTimeStamp}`
          })
        }, 500)
      })

    } else {
      console.log('empty');
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let addressList = wx.getStorageSync('addressList');
    let address = {};
    for (let k in addressList) {
      if (addressList[k].defaultAddressFlag) {
        address = addressList[k];
      }
    }
    let submitArrayData = JSON.parse(decodeURIComponent(options.data));
    let submitPrice = 0;
    
    for (let k in submitArrayData) {
      submitPrice += submitArrayData[k].totalPrice;
    }
    
    this.setData({
      address,
      submitArrayData,
      submitPrice
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  }
})