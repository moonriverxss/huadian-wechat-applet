// pages/manage/manage.js
import {getOrder} from '../../network/main';

Page({

  /**
   * 页面的初始数据
   */
  data: {
    openFlag: '1',
    addressList: [],
    orderList: []
  },
  // 关于我们
  aboutUs() {
    wx.navigateTo({
      url: `/pages/aboutUs/aboutUs`
    })
  },
  // 改变折叠选项
  changeFlag(event) {
    this.setData({
      openFlag: event.detail,
    });
  },
  // 点击改变默认地址
  changeDefault(event) {
    const index = event.currentTarget.id;
    const addressList = wx.getStorageSync('addressList');
    for (let k in addressList) {
      addressList[k].defaultAddressFlag = false;
    }
    addressList[index].defaultAddressFlag = true;
    wx.setStorageSync('addressList', addressList);
    this.onShow();
  },
  // 修改地址
  changeAddressDetail(event) {
    const index = event.currentTarget.id
    wx.navigateTo({
      url: `/pages/operationAddress/operationAddress?index=${index}`
    })
  },
  addAddress() {
    wx.navigateTo({
      url: `/pages/operationAddress/operationAddress`
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // wx.removeStorageSync('orderList');
    let orderListLocal = wx.getStorageSync('orderList');
    let handle = {};
    let orderList = [];
    for (let k in orderListLocal) {
      getOrder({
        orderTimeStamp: orderListLocal[k]
      }).then(res => {
        const {id, info, orderFlag, orderTime, products, totalPrice, orderTimeStamp} = res[0];
        handle = {
          id,
          info: JSON.parse(info),
          orderFlag: JSON.parse(orderFlag),
          orderTime,
          products: JSON.parse(products),
          totalPrice,
          orderTimeStamp
        }
        orderList.push(handle);
        this.setData({
          orderList
        })
      })
    }
    this.setData({
      addressList: wx.getStorageSync('addressList')
    })
  },
  toOrderDetail(event) {
    wx.navigateTo({
      url: `/pages/orderDetail/orderDetail?orderTimeStamp=${event.currentTarget.id}`
    })
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  }
})