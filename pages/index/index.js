import {getFlowerAllList} from '../../network/main';

Page({
  data: {
    active: 0,
    allList: [],
    tabList: [
      {id: 0, name: '花卉'},
      {id: 1, name: '树种'},
    ],
    offset: 0,
    size: 10,
    productId: 0,
  },
  // 改变tab栏时重置数据
  onChange(e) {
    this.setData({
      allList: [],
      active: e.detail.name,
      offset: 0
    });
    this.getFlowerAllList(this.data.active);
  },
  // 点击搜索框跳转页面
  onInput() {
    wx.navigateTo({
      url: `/pages/searchPage/searchPage`
    })
  },
  // 获取所有花卉
  getFlowerAllList(type) {
    getFlowerAllList({
      type,
      offset: this.data.offset,
      size: this.data.size
    }).then(res => {
      // console.log(res);
      let changeArray = res.result;
      // 静态数据
      if (type == 1) {
        this.setData({
          // 这里只有使用concat，push方法无法使用
          allList: res
        })
      } else {
        for (let i in changeArray) {
          let theCover = JSON.parse(changeArray[i].imgPath)[0].filename;
          changeArray[i].cover = theCover;
        }
        // console.log(this.data.active);
        this.setData({
          // 这里只有使用concat，push方法无法使用
          allList: this.data.allList.concat(changeArray)
        })
      }
    })
  },
  // 点击进入描述详情页面
  clickItem(e) {
    this.setData({
      productId: e.currentTarget.dataset.id
    })
    wx.navigateTo({
      url: `/pages/productDetail/productDetail?type=${this.data.active}&id=${this.data.productId}`
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // console.log(this.data.active);
    this.getFlowerAllList(this.data.active);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    // 接近底部时改变搜索发送偏移量
    this.setData({
      offset: this.data.offset + 10,
    })
    this.getFlowerAllList(this.data.active)
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: '花驿小栈首页',
      path: '/pages/index/index',
    }
  }
})