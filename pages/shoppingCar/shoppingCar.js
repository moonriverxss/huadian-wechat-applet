// pages/shoppingCar/shoppingCar.js
import Dialog from '@vant/weapp/dialog/dialog';
import Toast from '@vant/weapp/toast/toast';
import {getFlowerDetailById} from '../../network/main';

Page({

  /**
   * 页面的初始数据
   */
  data: {
    shoppingCarData: [],
    allSelectFlag: false,
    submitPrice: 0
  },

  // 操作页面数据
  changeAllData() {
    // wx.removeStorageSync('shoppingCarList');
    let dataArray = wx.getStorageSync('shoppingCarList');
    if (dataArray.length) {
      let shoppingCarData = [];
      // 通过本地存储id获取商品信息
      for (let k in dataArray) {
        const {type, id, theNum, checked} = dataArray[k];
        getFlowerDetailById({
          type,
          id
        }).then(res => {
          res.cover = JSON.parse(res.imgPath)[0].filename;
          res.totalPrice = theNum * res.price;
          res.theNum = theNum;
          res.checked = checked;
          shoppingCarData.push(res);
          this.setData({
            shoppingCarData
          })
        })
      }
    } else {
      this.setData({
        shoppingCarData: dataArray
      })
    }
    this.setData({
      submitPrice: 0
    })
  },
  // 查看商品详情
  toProductDetail(data) {
    const {type, id} = data.currentTarget.dataset.index;
    wx.navigateTo({
      url: `/pages/productDetail/productDetail?type=${type}&id=${id}`
    })
  },
  // 长按删除商品
  toDelete(event) {
    let index = event.target.id;
    Dialog.confirm({
      title: '警告',
      message: '是否删除当前商品？',
    }).then(() => {
      this.deleteProductFun(index);
    })
  },
  // 选择商品
  changeSelect(event) {
    const index = event.currentTarget.id;
    const flag = event.detail;
    let allSelectFlag = this.data.allSelectFlag;
    let shoppingCarData = this.data.shoppingCarData;
    let submitPrice = 0;

    // 改变选中状态
    shoppingCarData[index].checked = flag;
    // 未选中时重新计算价格
    for (let k in shoppingCarData) {
      if (shoppingCarData[k].checked) {
        submitPrice += shoppingCarData[k].totalPrice;
      } else {
        allSelectFlag = false;
      }
    }
    
    this.setData({
      allSelectFlag,
      shoppingCarData,
      submitPrice
    })
  },
  // 删除商品
  deleteProduct(data) {
    const {instance} = data.detail;
    // 获取商品索引并删除
    let index = data.currentTarget.dataset.index;
    this.deleteProductFun(index);
    // 回弹删除
    instance.close();
  },
  // 删除商品
  deleteProductFun(index) {
    // 获取老购物车数据
    let oldArrayData = this.data.shoppingCarData;
    // 删除老数据
    oldArrayData.splice(index, 1);
    // 更新新数据
    this.setData({
      shoppingCarData: oldArrayData
    })
    wx.setStorageSync('shoppingCarList', oldArrayData);
  },
  // 商品全选
  changeAllSelect(event) {
    let allSelectFlag = event.detail;
    let dataArray = this.data.shoppingCarData;
    let submitPrice = 0;
    
    // 循环改变价格
    for (let k in dataArray) {
      dataArray[k].checked = allSelectFlag;
      submitPrice += dataArray[k].totalPrice;
    }
    // 取消全选价格归零
    if (!event.detail) {
      submitPrice = 0;
    }
    
    this.setData({
      allSelectFlag,
      shoppingCarData: dataArray,
      submitPrice
    })
  },
  // 提交订单
  commitOrder(event) {
    let shoppingCarData = this.data.shoppingCarData;
    let submitOrderData = [];
    for (let k in shoppingCarData) {
      if (shoppingCarData[k].checked) {
        const {type, details, id, imgPath, name, theNum, totalPrice} = shoppingCarData[k];
        submitOrderData.push({
          type, 
          id, 
          name, 
          cover: JSON.parse(imgPath)[0].filename, 
          theNum, 
          totalPrice,
          details 
        })
      }
    }
    if (submitOrderData.length) {
      wx.navigateTo({
        url: `/pages/submitOrder/submitOrder?data=${encodeURIComponent(JSON.stringify(submitOrderData))}`
      })
    } else {
      Toast('请选择商品哦');
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.changeAllData();
    this.setData({
      allSelectFlag: false
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    this.changeAllData();
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.changeAllData();
    let shoppingCarData = this.data.shoppingCarData;
    // 改变选中状态
    for (let k in shoppingCarData) {
      shoppingCarData[k].checked = false;
    }
    
    setTimeout(() => {
      this.setData({
        allSelectFlag: false,
        shoppingCarData
      })
      wx.stopPullDownRefresh();
    }, 500);
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  }
})