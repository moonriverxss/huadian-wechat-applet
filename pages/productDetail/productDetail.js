const app = getApp();
import {getFlowerDetailById} from '../../network/main';
import Dialog from '@vant/weapp/dialog/dialog';

Page({
  /**
   * 页面的初始数据
   */
  data: {
    show: false,
    detailData: {},
    rotationImg: [],
    flowerDetailImg: [],
    nowRotationIndex: 0,
    showBox: false,
    theNum: 1,
    submitArray: [],
    submitFlag: true
  },
  onClickShow() {
    this.setData({show: true});
  },
  onClickHide() {
    this.setData({show: false});
  },
  // 轮播图改变时刷新下标
  changeIndex(value) {
    this.setData({
      nowRotationIndex: value.detail.current
    })
  },
  // 联系客服
  toService() {
    Dialog.alert({
      title: '提示',
      message: '此功能暂未开启',
    }).then(() => {
      // on close
    });
  },
  // 进入购物车
  shoppingCar() {
    wx.switchTab({
      url: `/pages/shoppingCar/shoppingCar`,
    })
  },
  // 加入购物车
  oppenShoppingCar() {
    this.setData({
      showBox: true,
      submitFlag: true
    })
  },
  // 直接购买
  buyProductNowFlag() {
    this.setData({
      showBox: true,
      submitFlag: false
    })
  },
  // 物品数量选择
  onChangeCount(event) {
    this.setData({
      theNum: event.detail
    })
  },  
  // 提交商品
  submitProduct() {
    let changeFlag = false;
    
    // 未找到本地对应文件就将其创建
    if (!wx.getStorageSync('shoppingCarList')) {
      wx.setStorageSync('shoppingCarList', []);
    }
    const {type, id} = this.data.detailData;
    let localShoppingCar = wx.getStorageSync('shoppingCarList');

    let addData = {
      type,
      id,
      theNum: this.data.theNum,
      checked: false
    }
    // 判断商品是否存在，存在就追加，不存在就添加
    for (let k in localShoppingCar) {
      if (localShoppingCar[k].id == id) {
        localShoppingCar[k].theNum += addData.theNum;
        changeFlag = true;

        break;
      } else {
        changeFlag = false;
      }
    }
    
    if (changeFlag) {
      wx.setStorageSync('shoppingCarList', localShoppingCar);
    } else {
      localShoppingCar.push(addData);
      wx.setStorageSync('shoppingCarList', localShoppingCar);
    }

    wx.showToast({
      title: '加入购物车成功！',
      icon: 'success',
      duration: 1500
    })
    setTimeout(() => {
      this.setData({
        showBox: false
      })
    }, 500);
  },
  // 立即购买
  buyProductNow() {
    const {type, details, id, imgPath, name, price} = this.data.detailData;
    let productItem = [{
      type,
      id,
      name, 
      cover: JSON.parse(imgPath)[0].filename, 
      theNum: this.data.theNum, 
      totalPrice: this.data.theNum * price,
      details 
    }]
    wx.navigateTo({
      url: `/pages/submitOrder/submitOrder?data=${encodeURIComponent(JSON.stringify(productItem))}`
    })
  },
  // 关闭购物车盒子
  onCloseBox() {
    this.setData({
      showBox: false
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const {type, id} = options;
    // const type = 0;
    // const id = 8;
    getFlowerDetailById({
      type,
      id
    }).then(res => {
      // console.log(res);
      let imgs = JSON.parse(res.imgPath);
      let rotationImg = [];
      let flowerDetailImg = [];
      for (let k in imgs) {
        if (imgs[k].fieldname == 'rotationImg') {
          rotationImg.push(imgs[k].filename);
        } else if (imgs[k].fieldname == 'flowerDetailImg') {
          flowerDetailImg.push(imgs[k].filename);
        }
      }
      this.setData({
        detailData: res,
        rotationImg,
        flowerDetailImg
      })
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    console.log(this.data.detailData)
    return {
      title: `${this.data.detailData.name}`,
      path: `/pages/productDetail/productDetail?type=${0}&id=${this.data.detailData.id}`,
    }
  }
})