// pages/aboutUs/aboutUs.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    longitude: 104.15444846577543,
    latitude: 30.980185800172013,
    markers: [
      {
        id: 0,
        iconPath: '../../images/destination/destination.png',
        longitude: '104.15444846577543',
        latitude: '30.980185800172013',
        width: '40px',
        height: '40px',
        callout: {
            color: '#ffffff',
            content: '点击图标进入腾讯地图开始导航',
            fontSize: 16,
            borderRadius: 5,
            bgColor: '#377D89',
            padding:10,
            textAlign:'center',
            display:"ALWAYS"
        }
      }
    ],
    aboutUsText: `<p style="text-align: center; line-height: 1.5em;">
    <span style="margin: 0px; padding: 0px; line-height: 16.8px; color: rgb(90, 90, 90); font-family: Arial, 宋体, Helvetica, sans-serif, Verdana; font-size: 16px;"></span>
</p>
<p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; text-align: center; line-height: 1.5em;">
    <span style="font-size: 16px; margin: 0px; padding: 0px; line-height: 16.8px; color: rgb(0, 0, 0); font-family: 微软雅黑;">您好,欢迎光临花驿小栈！</span>
</p>
<p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; text-align: center; line-height: 1.5em;">
    <span style="text-align: center; color: rgb(0, 0, 0); font-family: 微软雅黑; font-size: 16px;">花驿小栈是一个私人小型花店</span>
</p>
<p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; font-variant-numeric: normal; font-variant-east-asian: normal; line-height: 1.5em;">
    <span style="text-align: center; color: rgb(0, 0, 0); font-family: 微软雅黑; font-size: 16px;"><br/></span>
</p>
<p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; color: rgb(90, 90, 90); font-family: Arial, 宋体, Helvetica, sans-serif, Verdana; font-size: 12px; white-space: normal; text-align: center; line-height: 1.5em;">
    <span style="margin: 0px; padding: 0px; line-height: 16.8px; color: rgb(0, 0, 0); font-family: 微软雅黑; font-size: 16px;">生活艺术 不可复制&nbsp;</span>
</p>
<p style="margin-top: 0px; margin-bottom: 0px; padding: 0px; color: rgb(90, 90, 90); font-family: Arial, 宋体, Helvetica, sans-serif, Verdana; font-size: 12px; white-space: normal; text-align: center; line-height: 1.5em;">
    <span style="margin: 0px; padding: 0px; line-height: 16.8px; color: rgb(0, 0, 0); font-family: 微软雅黑; font-size: 16px;">我们只做品质</span>
</p>`
  },
  getMarker(event) {
    // console.log(event);
    const that = this;
    wx.getLocation({
        type: 'gcj02', 
        success: function (res) {
          wx.openLocation({
            latitude: that.data.latitude,
            longitude: that.data.longitude,
            name: "花驿小栈",
            address: '四川省彭州市濛阳镇成德大道'
          })
        }
      })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})