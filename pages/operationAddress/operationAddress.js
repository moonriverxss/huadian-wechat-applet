// pages/operationAddress/operationAddress.js
import Toast from '@vant/weapp/toast/toast';
import Dialog from '@vant/weapp/dialog/dialog';

Page({

  /**
   * 页面的初始数据
   */
  data: {
    name: '',
    phone: '',
    area: '',
    address: '',
    defaultAddressFlag: false,
    nowIndex: 0,
    changeFlag: false
  },
  changeDefaultAddressFlag(event) {
    this.setData({
      defaultAddressFlag: event.detail
    })
  },
  seaveAddress() {
    if (!wx.getStorageSync('addressList')) {
      wx.setStorageSync('addressList', []);
    }
    let addressList = wx.getStorageSync('addressList');
    let addressListItem = {};

    let name = this.data.name;
    let phone = this.data.phone;
    let area = this.data.area;
    let address = this.data.address;
    let defaultAddressFlag = this.data.defaultAddressFlag;
    
    // 判断当前是否设置为默认地址
    if (addressList.length && defaultAddressFlag == true) {
      for (let k in addressList) {
        addressList[k].defaultAddressFlag = false;
      }
      wx.setStorageSync('addressList', addressList);
    }

    // 判断与提交地址
    if (name != '' && phone != '' && area != '' && address != '') {
      addressListItem = {
        name, 
        phone, 
        area, 
        address, 
        defaultAddressFlag
      }

      if (this.data.changeFlag) {
        addressList[this.data.nowIndex] = addressListItem;
        wx.setStorageSync('addressList', addressList);
      } else {
        addressList.push(addressListItem);
        wx.setStorageSync('addressList', addressList);
      }

      Toast('保存成功！');
      setTimeout(() => {
        wx.switchTab({
          url: '/pages/manage/manage'
        })
      }, 500)
    } else {
      Toast('请将表格填写完整');
    }
  },
  delateAddress() {
    Dialog.confirm({
      message: '是否确定删除当前地址？',
    }).then(() => {
      let addressList = wx.getStorageSync('addressList');
      addressList.splice(this.data.nowIndex, 1);
      wx.setStorageSync('addressList', addressList);
      setTimeout(() => {
        wx.switchTab({
          url: '/pages/manage/manage'
        })
      }, 500)
    }).catch(() => {})
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const {index} = options;
    if (index) {
      let {name, phone, area, address, defaultAddressFlag} = wx.getStorageSync('addressList')[index];
      this.setData({
        name, 
        phone, 
        area, 
        address, 
        defaultAddressFlag,
        nowIndex: index,
        changeFlag: true
      })
    } else {
      console.log('error');
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // console.log(wx.getStorageSync('addressList'));
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  }
})