import http from "./http";

// 获取推荐花卉信息
const getFlowerRecommendList = data => {
  return http({
    url: '/flower/flowerRecommendList',
    data,
    method: "GET", 
  })
}
// 通过tabId显示展现区域
const selectOptions = data => {
  return http({
    url: '/selectOptions/selectType',
    data,
    method: "GET", 
  })
}
// 通过名称获取展示花卉树种列表分列数据
const getFlowerListByName = data => {
  return http({
    url: '/flower/flowerListByName/',
    data,
    method: "GET", 
  })
}
// 请求所有花卉树种
const getFlowerAllList = data => {
  return http({
    url: '/flower/flowerList',
    data,
    method: "GET", 
  })
}
// 通过花卉树种id获取花卉树种详情
const getFlowerDetailById = data => {
  return http({
    url: '/flower/flowerDetail',
    data,
    method: "GET", 
  })
}
// 提交订单
const submitOrder = data => {
  return http({
    url: '/order/submitOrder',
    data,
    method: "GET", 
  })
}
// 通过时间戳获取订单信息
  const getOrder = data => {
    return http({
      url: '/order/getOrder',
      data,
      method: "GET", 
    })
  }

module.exports = {
  getFlowerRecommendList,
  selectOptions,
  getFlowerListByName,
  getFlowerAllList,
  getFlowerDetailById,
  submitOrder,
  getOrder
}