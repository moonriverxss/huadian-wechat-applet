const LocalURL = "http://localhost:8080";
const OnlineURL = "https://api.moonriver.city"

module.exports = {
  LocalURL,
  OnlineURL
}