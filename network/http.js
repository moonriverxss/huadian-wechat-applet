import {LocalURL, OnlineURL} from "./baseUrl";

const requestFun = config => {
  return new Promise((resolve, reject) => {
    wx.request({
      // url: LocalURL + config.url,
      url: OnlineURL + config.url,
      timeout: 3000,
      data: config.data,
      method: config.method,
      success: res => {
          resolve(res.data)
      },
      fail(err) {
        console.log('err数据请求失败了', err);
      }
    })
  })
}

export default requestFun;